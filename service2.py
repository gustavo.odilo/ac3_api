from flask import Flask, jsonify
import requests

app = Flask(__name__)

@app.route('/get_todos', methods=['GET'])
def get_todos():
    response = requests.get('http://localhost:5000/todos')
    return jsonify(response.json())

@app.route('/post_todo', methods=['POST'])
def post_todo():
    response = requests.post('http://localhost:5000/todos')
    return response.text

@app.route('/delete_todo', methods=['DELETE'])
def delete_todo():
    response = requests.delete('http://localhost:5000/todos')
    return response.text

if __name__ == '__main__':
    app.run()