from flask import Flask, request, jsonify
import requests

app = Flask(__name__)

@app.route('/todos', methods=['GET', 'POST', 'DELETE'])
def todos():
    if request.method == 'GET':
        response = requests.get('https://jsonplaceholder.typicode.com/todos')
        return jsonify(response.json())
    elif request.method == 'POST':
        # Aqui você pode adicionar a lógica para processar a requisição POST
        return 'POST request received'
    elif request.method == 'DELETE':
        # Aqui você pode adicionar a lógica para processar a requisição DELETE
        return 'DELETE request received'

if __name__ == '__main__':
    app.run()